package clinic.BBDD;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Juan Pablo Peña F. - Juan Sebastián Sanín V.
 */
public class conectar {
      
    Connection conectar;
    
    public Connection conexion(){   
        
        try{
            Class.forName("com.mysql.jdbc.Driver");//.newInstance();
            conectar= DriverManager.getConnection("jdbc:mysql://localhost/clinic","root","MyNewPass");
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexion de la base de datos");
        }catch (ClassNotFoundException ex) {    }
        return conectar;
    }  
}

