package clinic.vistaSecretaria;

import clinic.BBDD.conectar;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Juan Pablo Peña F. - Juan Sebastián Sanín V.
 */
public class mostrarDatos {
    
    public void mostrarDatos(String valor, javax.swing.JTable tablaEstudiantes){
        
        conectar cc=new conectar();
        Connection cn=cc.conexion();
        DefaultTableModel modelo=new DefaultTableModel();
       
        modelo.addColumn("codigo");
        modelo.addColumn("nota"); 
        modelo.addColumn("correo"); 
        modelo.addColumn("contraseña"); 
        modelo.addColumn("nombre");
        modelo.addColumn("apellido");
        modelo.addColumn("telefono");
        modelo.addColumn("edad");
        modelo.addColumn("sexo");
          
        tablaEstudiantes.setModel(modelo);
        String sql = "";
        
        if (valor.equals("")) {
            sql="SELECT * FROM estudiante";
        }else {
            sql="SELECT * FROM estudiante WHERE (codigo='"+valor+"' OR correo='"+valor+"' OR nombre='"+valor+"' OR apellido='"+valor+"' OR telefono='"+valor+"' OR edad='"+valor+"' OR sexo='"+valor+"')";
        }  
        
        String []datos=new String [9];
        
        try{
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                datos[8]=rs.getString(9);

                modelo.addRow(datos);
            }
            tablaEstudiantes.setModel(modelo);
        } catch(SQLException ex) {
            Logger.getLogger(datos.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
   public void mostrarDatosPacientes(String valor, javax.swing.JTable tablaPacientes){
        
        conectar cc=new conectar();
        Connection cn=cc.conexion();
        DefaultTableModel modelo=new DefaultTableModel();
       
        modelo.addColumn("cedula");
        modelo.addColumn("corre"); 
        modelo.addColumn("nombre"); 
        modelo.addColumn("apellido"); 
        modelo.addColumn("telefono");
        modelo.addColumn("edad");
        modelo.addColumn("sexo");
        modelo.addColumn("procedimiento");
        modelo.addColumn("observación");
        modelo.addColumn("codigo_estudiante");
        modelo.addColumn("esatado");
          
        tablaPacientes.setModel(modelo);
        String sql = "";
        
        if (valor.equals("")) {
            sql="SELECT * FROM paciente";
        }else {
            sql="SELECT * FROM paciente WHERE (codigo_estudiante='"+valor+"' OR cedula='"+valor+"')";
        }  
        
        String []datos=new String [12];
        
        try{
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                datos[8]=rs.getString(9);
                datos[9]=rs.getString(10);
                datos[10]=rs.getString(11);
                

                modelo.addRow(datos);
            }
            tablaPacientes.setModel(modelo);
        } catch(SQLException ex) {
            Logger.getLogger(datos.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
   public void mostrarDatosProcedimiento(String valor, javax.swing.JTable tablaProcedimiento){
        
        conectar cc=new conectar();
        Connection cn=cc.conexion();
        DefaultTableModel modelo=new DefaultTableModel();
       
        modelo.addColumn("ID");
        modelo.addColumn("Nombre"); 
        modelo.addColumn("Tipo"); 
            modelo.addColumn("Descripción"); 
      
          
        tablaProcedimiento.setModel(modelo);
        String sql = "";
        
        if (valor.equals("")) {
            sql="SELECT * FROM procedimiento";
        }
        
        String []datos=new String [5];
        
        try{
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
               

                modelo.addRow(datos);
            }
            tablaProcedimiento.setModel(modelo);
        } catch(SQLException ex) {
            Logger.getLogger(datos.class.getName()).log(Level.SEVERE,null,ex);
        }
    }
}
