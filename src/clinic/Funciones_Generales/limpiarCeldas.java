package clinic.Funciones_Generales;

/**
 *
 * @author @author Juan Pablo Peña F. - Juan Sebastián Sanín V.
 */
public class limpiarCeldas {
    
    public void limpiarCeldas(javax.swing.JTextField Codigo,javax.swing.JTextField Nota,javax.swing.JTextField Correo,javax.swing.JTextField Contraseña, javax.swing.JTextField Nombre, javax.swing.JTextField Apellido, javax.swing.JTextField Telefono, javax.swing.JTextField Edad){
    
        Codigo.setText("");
        Nota.setText("");
        Correo.setText("");
        Contraseña.setText("");
        Nombre.setText("");
        Apellido.setText("");
        Telefono.setText("");
        Edad.setText("");     
    }    
}
